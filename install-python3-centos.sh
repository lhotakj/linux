sudo su
sudo yum -y install https://centos7.iuscommunity.org/ius-release.rpm
sudo yum -y install python36u
sudo yum -y install python36u-pip
sudo yum -y install python36u-devel
echo "# python3 alias" >> /etc/bashrc
echo "alias python3\"=/usr/bin/python3.6\"" >> /etc/bashrc
