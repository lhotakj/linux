#!/bin/bash
# https://www.tecmint.com/install-samba4-on-centos-7-for-file-sharing-on-windows/ 
sudo yum install -y samba samba-client samba-common
sudo firewall-cmd --permanent --zone=public --add-service=samba
sudo firewall-cmd --reload
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.orig
sudo mkdir -p /srv/public
sudo chmod -R 0775 /srv/public
sudo chown -R nobody:nobody /srv/public
sudo chcon -t samba_share_t /srv/public
sudo chmod -R u+rwX,g+rwX,o-rwx /srv/public
echo -e '\n' >> /etc/samba/smb.conf
echo -e '[public]' >> /etc/samba/smb.conf
echo -e '\tcomment = public share' >> /etc/samba/smb.conf
echo -e '\tpath = /srv/public' >> /etc/samba/smb.conf
echo -e '\tbrowsable = yes' >> /etc/samba/smb.conf
echo -e '\twritable = yes' >> /etc/samba/smb.conf
echo -e '\tread only = no' >> /etc/samba/smb.conf
echo -e '\tforce user = nobody' >> /etc/samba/smb.conf
echo -e '\tforce group = nogroup' >> /etc/samba/smb.conf
echo -e '\tpublic = yes' >> /etc/samba/smb.conf
echo -e '\tusershare owner only = false' >> /etc/samba/smb.conf
echo -e '\tcreate mask = 0777' >> /etc/samba/smb.conf
echo -e '\tdirectory mask = 0777' >> /etc/samba/smb.conf
echo -e '\tguest ok = yes' >> /etc/samba/smb.conf

sudo systemctl enable smb.service
sudo systemctl enable nmb.service
sudo systemctl start smb.service
sudo systemctl start nmb.service
HOST=$(hostname -I)
HOST_=`echo $HOST`
echo " Samba ready! Try \\\\${HOST_}\\public"

# smbpasswd -a root
